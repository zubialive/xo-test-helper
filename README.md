# xo-test-helper

The Test-Helper is a library that provides the core functionality

## Installation
```
npm install --save-dev xo-test-helper
```

## Usage

```javascript
var XoTestHelper = require('xo-test-helper');
var xoTestHelper = new XoTestHelper();

/*
    you have access to :

    xoTestHelper.chai;
    xoTestHelper.sinon;
    xoTestHelper.expect;
*/

var XoTestHelper = require('xo-test-helper'),
    xoTestHelper = new XoTestHelper(),
    server;

describe('HEALTH-PLUGIN ', function() {
    before(function(done) {
        /* all keys except pluginPath are optional. just add what you need */

        server = new xoTestHelper.server({
            pluginPath : __dirname + '/../',
            stubsPath  : __dirname + '/stubs',
            useStubs   : true, // this defaults to false
            optionsPath: __dirname + '/mocks/options',
            port       : 5000
        });

        /* this is OPTIONAL!!!. Only used when you want to register an aditional plugin */
        server.registerPlugin({
            plugin: 'hapi-identities-crud',
            options: {
                key: value
            }
        });

        server.start(done);
    });

    after(function(done) {
        server.stop(done);
    });

    /* .... */
    }
});
```
[back to Top](#xo-test-helper)

# Documentations
[ShouldJs](http://chaijs.com/api/bdd)
[Expect](http://chaijs.com/api/bdd)
[Chai-as-Promised](http://chaijs.com/plugins/chai-as-promised)
[Sinon](http://ricostacruz.com/cheatsheets/sinon-chai.html)
[Chai](http://chaijs.com/plugins)
[Mocha](https://mochajs.org)

# Development
On your machine, we can initialize some data store dependencies to facilitate integration test.

## Methods
### .startupElastic
```javascript
/* Options param that may or may not be received */
forcedOptions = forcedOptions || {};

var options = {
    indexesPath      : forcedOptions.indexesPath || indexesPath,
    dataDirectoryPath: forcedOptions.dataDirectoryPath || dataDirectoryPath,
    connectionURL    : forcedOptions.connectionURL
};

/*
 * @param {Object} options
 *
 * @return {Promise}
 */
xoTestHelper.startupElastic(options).then(function (result) {
    server.start(done);
});
```

### .startupPostgres
```javascript
var options = {
    /* Absolute file path to postgres credentials and config needed to create the connection string */
    configPath       : configPath,
    /* Absolute path to a directory containing all postgres dump files (native dump files, see sample code) */
    dataDirectoryPath: dataDirectoryPath
};

/*
 * @param {Object} options
 *
 * @return {Promise}
 */
xoTestHelper.startupPostgres(options).then(function (result) {
    server.start(done);
});
```

### .proxyquire
```javascript
var pluginPath = __dirname + '/path/to/plugin';
var stubPath   = __dirname + '/path/to/stubs/folder';

/*
 * @param {String} pluginPath
 * @param {String} stubPath
 * @param {Object, optional} options
 
 * options object : {
 *  //default
 *  noCallThru : true,
 *  noPreserveCache : true
 * }

 
 * @return {Function}
 */
xoTestHelper.proxyquire(pluginPath, stubPath, options);
```


## Mocha Unit/Integration Test
Creates a docker compose environment with Elasticsearch, RabbitMQ and Postgres with a container testing code in watch mode.
```Text
make run
```

## Code Coverage
Removes node_modules folder and reinstalls from shrinkwrap or package.json
```Text
make cover
open coverage/lcov-report/*.html
```

## Install Packages
Removes node_modules folder and reinstalls from shrinkwrap or package.json
```Text
make install
```

## Create New Shrinkwrap From Scratch
Removes node_modules folder and npm_shrinkwrap.json and recreates from package.json using the latest from npm.
```Text
make clean
```

## Test with Mocha
Initiate mocha in watch mode on your local machine.  This will not boot up any docker resources so your integration test will fail.  But if you have postgres and elasticsearch containers running and hosted correctly, then these test will work.
```Text
make test
```

[back to top](#xo-test-helper)
