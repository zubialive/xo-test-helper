'use strict';

var Hoek = require('hoek');

var Plugin = function Plugin() {
    require('./helpers/initialize');
};

Hoek.merge(Plugin.prototype, {
    expect         : require('./methods/expect'),
    chai           : require('chai'),
    sinon          : require('sinon'),
    server         : require('./implementations/server.js'),
    startupElastic : require('./methods/startup-elastic'),
    startupPostgres: require('./methods/startup-postgres'),
    proxyquire     : require('./methods/proxyquire')
});

/**
 * Export the Instance to the World
 */
module.exports = Plugin.bind({});
