'use strict';

var curl    = require('curlrequest');
var fs      = require('fs');
var Promise = require('bluebird');

var executeCurl = function(method, data, url, urlAppend) {
    var options = {
        url   : url + '/' + urlAppend,
        data  : JSON.stringify(data),
        method: method
    };

    return new Promise(function(resolve, reject) {
        curl.request(options, function(error, parts) {
            if (error) {
                return reject(error);
            }

            try {
                parts = JSON.parse(parts);
            } catch (e) {
                return reject(e);
            }

            if (!parts) {
                return reject('No response');
            }

            if (parts.error) {
                return reject(parts.error);
            }

            return resolve(parts);
        });
    });
};

var deleteIndexes = function(self) {
    var promises = [];

    for (var key in self.indexes) {
        if (self.indexes.hasOwnProperty(key) === true) {
            promises.push(executeCurl('DELETE', self.indexes[key], self.connectionURL, key));
        }
    }

    return Promise.all(promises);
};

var processIndexes = function() {
    var promises = [];
    var self     = this;
    var indexes  = self.indexes;

    for (var key in indexes) {
        if (indexes.hasOwnProperty(key) === true) {
            promises.push(executeCurl('POST', indexes[key], self.connectionURL, key));
        }
    }

    return Promise.all(promises);
};

var processDirectoryFile = function(file) {
    var data;
    var promises = [];
    var self     = this;

    try {
        data = require(file);
    } catch (e) {
        return Promise.reject(e);
    }

    var processArray = function(firstKey, secondKey) {
        var curlPromise, urlAppend;

        for (var i = 0; i < data[firstKey][secondKey].length; i++) {
            var doc = data[firstKey][secondKey][i],
                id  = doc.id ? doc.id : (i + 1);

            urlAppend   = firstKey + '/' + secondKey + '/' + id;
            curlPromise = executeCurl('PUT', doc, self.connectionURL, urlAppend);
            promises.push(curlPromise);
        }
    };

    var processSecondKeys = function(firstKey) {
        for (var secondKey in data[firstKey]) {
            if (data[firstKey].hasOwnProperty(secondKey) === true) {
                processArray(firstKey, secondKey);
            }
        }
    };

    var processFirstKeys = function() {
        for (var firstKey in data) {
            if (data.hasOwnProperty(firstKey) === true) {
                processSecondKeys(firstKey);
            }
        }
    };

    processFirstKeys();

    return Promise.all(promises)
        .then(function() {
            //force indexes refresh
            return executeCurl('POST', null, self.connectionURL, '_refresh');
        });
};

var processDirectoryFiles = function() {
    var self = this;

    return new Promise(function(resolve, reject) {
        fs.readdir(self.dataDirectoryPath, function(error, files) {
            var promises = [];

            if (error) {
                return reject(error);
            }

            files.forEach(function(file) {
                promises.push(processDirectoryFile.call(self, self.dataDirectoryPath.replace(/\/$/, '') + '/' + file));
            });

            return Promise.all(promises).then(resolve, reject);
        });
    });
};
var elasticTimeout        = function(results) {
    return new Promise(function(resolve) {
        setTimeout(function() {
            resolve(results);
        }, 250);
    });
};

var errorHandler = function(error) {
    console.error(error);

    return Promise.reject(error);
};

module.exports = function(options) {
    var defaultConnectionURL = 'http://esbox:9200',
        indexes;

    if (!options) {
        return Promise.reject('No config object provided');
    }

    try {
        indexes = require(options.indexesPath);
    } catch (e) {
        return Promise.reject(e);
    }

    var dependencies = {
        indexes          : indexes,
        dataDirectoryPath: options.dataDirectoryPath,
        connectionURL    : options.connectionURL || defaultConnectionURL
    };

    return deleteIndexes(dependencies)
        .bind(dependencies)
        .then(processIndexes)
        .catch(processIndexes)
        .then(processDirectoryFiles)
        .then(elasticTimeout)
        .catch(errorHandler);
};
