'use strict';

var proxyquire = require('proxyquire'),
    Stubs      = require('../implementations/stubs'),
    stubs      = new Stubs();

module.exports = function(pluginPath, stubPath, options) {
    stubs.setStubsPath(stubPath);

    if (!options) {
        proxyquire.noCallThru();
    } else if (options.noCallThru) {
        proxyquire.noCallThru();
    } else if (options.noPreserveCache) {
        proxyquire.noPreserveCache();
    }

    return proxyquire(pluginPath, stubs.getStubs());
};
