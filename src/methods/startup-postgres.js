'use strict';

var fs       = require('fs');
var Promise  = require('bluebird');
var postgres = require('pg');
var squel    = require('squel').useFlavour('postgres');

var _options     = {};
var _connections = {};

postgres.defaults.poolIdleTimeout    = 10;
postgres.defaults.reapIntervalMillis = 10;

var getConnection = function(database) {
    return new Promise(function callback(resolve, reject) {
        if (typeof _connections[database] !== 'undefined') {
            return resolve(_connections[database]);
        }

        var config           = _options.config;
        var connectionString = config.protocol + '://' + config.user + ':' + config.pass + '@' + config.host + ':' + config.port + '/' + database;

        postgres.connect(connectionString, function callback(error, client, done) {
            var connection = {};

            if (error) {
                return reject(error);
            }

            connection = {
                client: client,
                done  : done
            };

            _connections[database] = connection;

            return resolve(connection);
        });
    });
};

var executeQuery = function(database, query) {
    return new Promise(function(resolve, reject) {
        getConnection(database).then(function(connection) {
            connection.client.query(query, function callback(error, result) {
                if (error) {
                    return reject(error);
                }

                return resolve(result.rows);
            });
        });
    });
};

var getListOfDatabases = function() {
    return new Promise(function(resolve, reject) {
        fs.readdir(_options.dataDirectoryPath, function(error, files) {
            var databases = [];

            if (error) {
                return reject(error);
            }

            files.forEach(function(file) {
                var fileDataPath   = _options.dataDirectoryPath.replace(/\/$/, '') + '/' + file;
                var payloadContent = require(fileDataPath);

                Object.keys(payloadContent).forEach(function(database) {
                    databases.push(database);
                });
            });

            return resolve(databases);
        });
    });
};

var dropDatabases = function(databases) {
    return new Promise(function(resolve) {
        var promises = [],
            query;

        databases.forEach(function(database) {
            query = 'DROP DATABASE IF EXISTS ' + database;
            promises.push(executeQuery('postgres', query));
        });

        Promise.all(promises).then(function() {
            return resolve(databases);
        });
    });
};

var createDatabases = function(databases) {
    return new Promise(function(resolve) {
        var promises = [],
            query;

        databases.forEach(function(database) {
            query = 'CREATE DATABASE ' + database;
            promises.push(executeQuery('postgres', query));
        });

        Promise.all(promises).then(function() {
            return resolve(databases);
        });
    });
};

var importObjects = function(database, table, objects) {
    return new Promise(function(resolve) {
        var promises = [];

        objects.forEach(function(object) {
            var query = squel.insert({
                replaceSingleQuotes: true
            });

            query.into(table);
            query.set('data', JSON.stringify(object));

            promises.push(executeQuery(database, query.toString()));
        });

        Promise.all(promises).then(function() {
            return resolve();
        });
    });
};

var prepareTables = function(database, tables) {
    return new Promise(function(resolve) {
        var promises = [];

        Object.keys(tables).forEach(function(table) {
            var queryCreateTable = 'CREATE TABLE ' + table + ' (data jsonb NOT NULL)';

            promises.push(executeQuery(database, queryCreateTable).then(function() {
                return importObjects(database, table, tables[table]);
            }));
        });

        Promise.all(promises).then(function() {
            return resolve();
        });
    });
};

var importData = function() {
    return new Promise(function(resolve, reject) {
        fs.readdir(_options.dataDirectoryPath, function(error, files) {
            var promises = [];

            if (error) {
                return reject(error);
            }

            files.forEach(function(file) {
                var fileDataPath = _options.dataDirectoryPath.replace(/\/$/, '') + '/' + file;
                var payloadData  = require(fileDataPath);

                Object.keys(payloadData).forEach(function(database) {
                    var tables = payloadData[database];

                    promises.push(prepareTables(database, tables));
                });
            });

            Promise.all(promises).then(function() {
                return resolve();
            });
        });
    });
};

var closeConnections = function() {
    Object.keys(_connections).forEach(function(connectionName) {
        _connections[connectionName].done();
    });
    _connections = {};
};

var errorHandler = function(error) {
    console.error(error);

    return Promise.reject(error);
};

module.exports = function(options) {
    _options = {
        config           : require(options.configPath),
        dataDirectoryPath: options.dataDirectoryPath
    };

    return getListOfDatabases()
        .then(dropDatabases)
        .then(createDatabases)
        .then(importData)
        .then(closeConnections)
        .then(function() {
            return Promise.resolve(true);
        })
        .catch(errorHandler);
};
