'use strict';

var fs = require('fs');

var Stubs = function() {
};

Stubs.prototype.setStubsPath = function(path) {
    this.stubsPath = path;
};

Stubs.prototype.getStubs = function(path) {
    var stubFiles;
    var stubs = {};
    var self  = this;

    if (path) {
        this.setStubsPath(path);
    }

    stubFiles = fs.readdirSync(this.stubsPath);

    stubFiles.forEach(function callback(fileName) {
        var stubName = fileName.replace(/\.[^\.]+$/, '');
        var stub     = require(self.stubsPath + '/' + stubName);

        if (stub instanceof Object) {
            stub['@global'] = true;
        } else {
            stub.prototype['@global'] = true;
        }

        stubs[stubName] = stub;
    });

    return stubs;
};

module.exports = Stubs.bind({});
