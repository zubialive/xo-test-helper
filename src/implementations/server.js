'use strict';

var server,
    proxyquire = require('proxyquire').noCallThru(),
    Hapi       = require('hapi'),
    Stubs      = require('./stubs'),
    Promise    = require('bluebird'),
    Hoek       = require('hoek'),
    stubs      = new Stubs();

var defaultOptions = {
    useStubs: false,
    port    : 5000
};

var Server = function(options) {
    var self = this;

    if (!options.pluginPath) {
        throw new Error('pluginPath needs to be defined');
    }

    self.options = Hoek.merge(defaultOptions, options || {});

    server = new Hapi.Server();
    server.connection({
        port: self.options.port
    });

    if (self.options.useStubs) {
        if (!self.options.stubsPath) {
            throw new Error('If Stubs are enabled, stubsPath needs to be provided');
        }

        stubs.setStubsPath(self.options.stubsPath);
        self.cachedStubs = stubs.getStubs();
    }

    self.register = [
        {
            register: (self.options.useStubs) ? proxyquire(self.options.pluginPath, self.cachedStubs) : require(self.options.pluginPath),
            options : (self.options.optionsPath) ? require(self.options.optionsPath) : {}
        }
    ];
};

Server.prototype.registerPlugin = function(plugin) {
    this.register.push(plugin);
};

Server.prototype.start = function(done) {
    server.register(this.register, function callback(error) {
        if (error) {
            throw Error(error);
        }
        server.start(done);
    });
};

Server.prototype.stop = function(done) {
    server.stop(done);
};

Server.prototype.inject = function(options) {
    return new Promise(function(resolve, reject) {
        try {
            server.inject(options, resolve);
        } catch (e) {
            reject(e);
        }
    });
};

Server.prototype.getStubs = function() {
    if (this.options.useStubs) {
        return this.cachedStubs;
    }

    return null;
};

module.exports = Server;
