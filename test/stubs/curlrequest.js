'use strict';

module.exports.request = function(options, callback) {
    if (typeof callback === 'function') {
        setTimeout(function() {
            callback(false, true);
        });
    }
};
