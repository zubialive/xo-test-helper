/*jshint -W030 */
'use strict';

var XoTestHelper = require(__dirname + '/../'),
    curl         = require('curlrequest'),
    xoTestHelper = new XoTestHelper();

var server;

describe('xo-test-helper ', function() {
    this.timeout(10000);

    describe('server', function() {
        it('should initialize server without failure using passed values', function() {
            (function() {
                server = new xoTestHelper.server({
                    pluginPath   : __dirname + '/mocks/plugin.js',
                    stubsPath    : __dirname + '/stubs',
                    doNotUseStubs: false,
                    optionsPath  : __dirname + '/mocks/options',
                    port         : 5000
                });
            }).should.not.throw();
        });

        it('should start the server without failure', function(done) {
            (function() {
                server.start(done);
            }).should.not.throw();
        });

        it('should stop the server without failure', function(done) {
            (function() {
                server.stop(done);
            }).should.not.throw();
        });
    });

    describe('startupElastic', function() {
        var requestSpy;

        var generateOptions = function(forcedOptions) {
            var indexesPath       = __dirname + '/data/elasticsearch/indexes.json';
            var dataDirectoryPath = __dirname + '/data/elasticsearch/payloads';

            forcedOptions = forcedOptions || {};

            var options = {
                indexesPath      : forcedOptions.indexesPath || indexesPath,
                dataDirectoryPath: forcedOptions.dataDirectoryPath || dataDirectoryPath,
                connectionURL    : forcedOptions.connectionURL
            };

            return options;
        };

        beforeEach(function() {
            requestSpy = xoTestHelper.sinon.spy(curl, 'request');

        });

        afterEach(function() {
            requestSpy.reset();
            requestSpy.restore();
        });

        it('should throw error when it doesn\'t receive a config variable', function() {
            return xoTestHelper.startupElastic().catch(function() {
            }).should.eventually.reject;
        });
        it('should use default connectionURL when none is provider', function() {
            var options = generateOptions({
                connectionURL: null
            });

            return xoTestHelper.startupElastic(options).then(function() {
                requestSpy.getCall(0).args[0].url.match('http://esbox:9200').should.have.length(1);
            });
        });
        it('should throw error when an improper connectionURL is used', function() {
            var options = generateOptions({
                connectionURL: 'http:/'
            });

            return xoTestHelper.startupElastic(options).should.be.rejected;
        });
        it('should throw error when an non 404 connectionURL is used', function() {
            var options = generateOptions({
                connectionURL: 'http://localhost'
            });

            return xoTestHelper.startupElastic(options).should.be.rejected;
        });
        it('should throw error when an improper indexPath is used', function() {
            var options = generateOptions({
                indexesPath: '/'
            });

            return xoTestHelper.startupElastic(options).should.be.rejected;
        });
        it('should throw error when an improper dataDirectoryPath is used', function() {
            var options = generateOptions({
                dataDirectoryPath: '@'
            });

            return xoTestHelper.startupElastic(options).should.be.rejected;
        });
        it('should throw error when an improper payload json is used', function() {
            var options = generateOptions({
                dataDirectoryPath: __dirname + '/data/elasticsearch/rotten-payloads'
            });

            return xoTestHelper.startupElastic(options).should.be.rejected;
        });

        it('should send curl requests with data to the elastic search endpoints', function() {
            var options = generateOptions();

            var indexes  = require(options.indexesPath);
            var payload1 = require(options.dataDirectoryPath + '/payload1.json');
            var payload2 = require(options.dataDirectoryPath + '/payload2.json');

            return xoTestHelper.startupElastic(options).then(function() {
                requestSpy.getCall(2).args[0].data.should.deep.equal(JSON.stringify(indexes.twitter));
                requestSpy.getCall(3).args[0].data.should.deep.equal(JSON.stringify(indexes.facebook));
                requestSpy.getCall(4).args[0].data.should.deep.equal(JSON.stringify(payload1.twitter.tweet[0]));
                requestSpy.getCall(5).args[0].data.should.deep.equal(JSON.stringify(payload1.twitter.tweet[1]));
                requestSpy.getCall(6).args[0].data.should.deep.equal(JSON.stringify(payload2.facebook.feed[0]));
                requestSpy.getCall(7).args[0].data.should.deep.equal(JSON.stringify(payload2.facebook.feed[1]));
            });
        });
    });

    describe('startupPostgres', function() {
        it('should be possible to startup postgres', function() {
            var configPath        = __dirname + '/data/postgres/config.json',
                dataDirectoryPath = __dirname + '/data/postgres/paylods';

            var options = {
                configPath       : configPath,
                dataDirectoryPath: dataDirectoryPath
            };

            return xoTestHelper.startupPostgres(options)
                .then(function(result) {
                    return result.should.equal(true);
                });
        });
    });

    describe('proxyquire', function() {
        it('should call proxyquire method', function() {
            var options        = {
                pluginPath: __dirname + '/mocks/proxyquire-test',
                stubPath  : __dirname + '/stubs'
            };
            var curl           = require('./stubs/curlrequest');
            var proxyquireTest = xoTestHelper.proxyquire(options.pluginPath, options.stubPath);

            proxyquireTest().should.be.equal(curl);
        });

        it('should call proxyquire method with noCallThru feature', function() {
            var options        = {
                pluginPath: __dirname + '/mocks/proxyquire-test',
                stubPath  : __dirname + '/stubs'
            };
            var curl           = require('./stubs/curlrequest');
            var proxyquireTest = xoTestHelper.proxyquire(options.pluginPath, options.stubPath, {
                noCallThru: true
            });

            proxyquireTest().should.be.equal(curl);
        });

        it('should call proxyquire method with noPreserveCache feature', function() {
            var options        = {
                pluginPath: __dirname + '/mocks/proxyquire-test',
                stubPath  : __dirname + '/stubs'
            };
            var curl           = require('./stubs/curlrequest');
            var proxyquireTest = xoTestHelper.proxyquire(options.pluginPath, options.stubPath, {
                noPreserveCache: true
            });

            proxyquireTest().should.be.equal(curl);
        });
    });

    it('should be possible to call sinon', function() {
        var sinon;

        (function() {
            sinon = xoTestHelper.sinon;
        }).should.not.throw();

        (sinon).should.be.an('object');
    });

    it('should be possible to call expect', function() {
        var expect;

        (function() {
            expect = xoTestHelper.expect;
        }).should.not.throw();

        (expect).should.be.a('function');
    });

    it('should be possible to call chai', function() {
        var chai;

        (function() {
            chai = xoTestHelper.chai;
        }).should.not.throw();

        (typeof chai).should.equal('object');
    });
});
